//
//  FirstViewController.swift
//  BillionQuestions
//
//  Created by Tamas Szikszai on 17/05/2015.
//  Copyright (c) 2015 Tamas Szikszai. All rights reserved.
//

import UIKit

class PlayViewController: UIViewController {

    @IBOutlet weak var nextQuestionButton: UIButton!
    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var answer4Button: UIButton!
    @IBOutlet weak var answer3Button: UIButton!
    @IBOutlet weak var answer2Button: UIButton!
    @IBOutlet weak var answer1Button: UIButton!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var countDownTimerLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var categoryLabel: UILabel!
    
    var timer = NSTimer()
    var seconds = 30
    var currentCorrectAnswer = 0
    var currentQuestionId = 0

    let userId = NSUserDefaults.standardUserDefaults().valueForKey("userId") as! String
    
    func hideQuestion(){
        loadingLabel.hidden = false
        answer1Button.hidden = true
        answer2Button.hidden = true
        answer3Button.hidden = true
        answer4Button.hidden = true
        questionLabel.hidden = true
        countDownTimerLabel.hidden = true
        nextQuestionButton.hidden = true
        categoryLabel.hidden = true
    }
    
    func loadQuestion() {
        self.hideQuestion()
        
        var url : String = "http://billion.lepunk.co.uk/api/"
        var request : NSMutableURLRequest = NSMutableURLRequest()
        var bodyData = "method=question&user_id=\(self.userId)"
        request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding);
        request.URL = NSURL(string: url)
        request.HTTPMethod = "POST"
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
            
            if (data == nil || error != nil){
                dispatch_async(dispatch_get_main_queue(), {
                    self.loadingLabel.text = "Network error"
                })
            } else {
                let jsonResult: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error: error) as? NSDictionary
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.renderQuestion(jsonResult)
                })
            }
            
        })
    }
    
    func enableButtons() {
        self.answer1Button.enabled = true
        self.answer2Button.enabled = true
        self.answer3Button.enabled = true
        self.answer4Button.enabled = true
        
        self.answer1Button.backgroundColor = UIColor(rgba: "#0091FF")
        self.answer2Button.backgroundColor = UIColor(rgba: "#0091FF")
        self.answer3Button.backgroundColor = UIColor(rgba: "#0091FF")
        self.answer4Button.backgroundColor = UIColor(rgba: "#0091FF")
    }
    
    func disableButtons(){
        self.answer1Button.enabled = false
        self.answer2Button.enabled = false
        self.answer3Button.enabled = false
        self.answer4Button.enabled = false
    }
    
    func renderQuestion(jsonResult: NSDictionary!){
        let question = jsonResult["question"] as? String
        let category = jsonResult["category"] as! String
        let answers = jsonResult["answers"] as! [String]
        self.currentCorrectAnswer = jsonResult["correct"] as! Int
        self.currentQuestionId = jsonResult["id"] as! Int
            
        self.loadingLabel.hidden = true
        
        self.categoryLabel.text = category
        self.categoryLabel.hidden = false
        
        self.questionLabel.text = question
        self.questionLabel.hidden = false
            
        self.answer1Button.setTitle(answers[0], forState: UIControlState.Normal)
        self.answer1Button.hidden = false
            
        self.answer2Button.setTitle(answers[1], forState: UIControlState.Normal)
        self.answer2Button.hidden = false
            
        self.answer3Button.setTitle(answers[2], forState: UIControlState.Normal)
        self.answer3Button.hidden = false
            
        self.answer4Button.setTitle(answers[3], forState: UIControlState.Normal)
        self.answer4Button.hidden = false
            
        self.countDownTimerLabel.hidden = false
        
        self.enableButtons()
        self.startCountDown()
    }
    
    func subtractTime(){
        self.seconds--
        countDownTimerLabel.text = "\(self.seconds)s"
        
        let fractionalProgress = Float(self.seconds) / 30.0
        let animated = self.seconds != 0
        self.progressBar.setProgress(fractionalProgress, animated: animated)
        
        
        if (self.seconds == 0) {
            self.timer.invalidate()
            self.disableButtons()
            self.highlightCorrectAnswer()
            
            countDownTimerLabel.text = "Out of Time"
            nextQuestionButton.hidden = false
            self.logResult(-1, correct: 0)
        }
    }
    
    func startCountDown(){
        self.seconds = 30
        countDownTimerLabel.text = "\(self.seconds)s"
        countDownTimerLabel.textColor = UIColor.darkGrayColor()
        countDownTimerLabel.font = UIFont(name: countDownTimerLabel.font.fontName, size: 20)
        
        self.timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("subtractTime"), userInfo: nil, repeats: true)
        
    }
    
    func logResult(answer: Int, correct: Int){
        var url : String = "http://billion.lepunk.co.uk/api/"
        var request : NSMutableURLRequest = NSMutableURLRequest()
        var bodyData = "method=answer&user_id=\(self.userId)&question_id=\(self.currentQuestionId)&answer=\(answer)&correct=\(correct)"
        println(bodyData)
        request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding);
        request.URL = NSURL(string: url)
        request.HTTPMethod = "POST"
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse!, data: NSData!, error: NSError!) -> Void in
        })
    }
    
    func highlightCorrectAnswer(){
        switch(self.currentCorrectAnswer){
            case 0: self.answer1Button.backgroundColor = UIColor(rgba: "#00aa00")
            case 1: self.answer2Button.backgroundColor = UIColor(rgba: "#00aa00")
            case 2: self.answer3Button.backgroundColor = UIColor(rgba: "#00aa00")
            case 3: self.answer4Button.backgroundColor = UIColor(rgba: "#00aa00")
            default:  self.answer1Button.backgroundColor = UIColor(rgba: "#00aa00")
        }
    }
    
    @IBAction func nextQuestion(sender: AnyObject) {
        self.loadQuestion()
    }
    
    @IBAction func sendAnswer(sender: UIButton) {
        let buttonIndex = sender.tag
        var correct = 0
        
        self.timer.invalidate()
        self.disableButtons()
        
        if (buttonIndex == self.currentCorrectAnswer){
            correct = 1
            sender.backgroundColor = UIColor(rgba: "#00aa00")
        } else {
            sender.backgroundColor = UIColor(rgba: "#aa0000")
            self.highlightCorrectAnswer()
        }
        
        nextQuestionButton.hidden = false
        
        logResult(buttonIndex, correct: correct)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.tabBarController?.title = "Play BillionQuestions"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view, typically from a nib.
        self.loadQuestion()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}