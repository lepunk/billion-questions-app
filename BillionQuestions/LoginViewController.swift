//
//  SecondViewController.swift
//  BillionQuestions
//
//  Created by Tamas Szikszai on 17/05/2015.
//  Copyright (c) 2015 Tamas Szikszai. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    @IBOutlet weak var usernameErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var userNameInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if (FBSDKAccessToken.currentAccessToken() != nil){
            // User is already logged in, do work such as go to next view controller.
            self.returnUserData()
            let loginView : FBSDKLoginButton = FBSDKLoginButton()
            self.view.addSubview(loginView)
            loginView.center = self.view.center
            loginView.readPermissions = ["public_profile", "email", "user_friends"]
            loginView.delegate = self
            
        } else {
            let loginView : FBSDKLoginButton = FBSDKLoginButton()
            self.view.addSubview(loginView)
            loginView.center = self.view.center
            loginView.readPermissions = ["public_profile", "email", "user_friends"]
            loginView.delegate = self
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Facebook Delegate Methods
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        
        if ((error) != nil){
            // Process error
        } else if result.isCancelled {
            // Handle cancellations
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if result.grantedPermissions.contains("email") {
                self.returnUserData()
                // Do work
            }
        }
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {

    }
    
    func returnUserData() {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            
            if ((error) != nil) {
                // Process error
            } else {
                let facebookId : String = result.valueForKey("id") as! String
                var url : String = "http://billion.lepunk.co.uk/api/"
                var request : NSMutableURLRequest = NSMutableURLRequest()
                var bodyData = "method=facebookLogin&facebook_id=\(facebookId)"
                request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding);
                request.URL = NSURL(string: url)
                request.HTTPMethod = "POST"
                    
                NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse!, data: NSData!, error: NSError!) -> Void in
                    var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
                        
                    var statusCode = 0
                    if let httpResponse = response as? NSHTTPURLResponse {
                        statusCode = httpResponse.statusCode
                    }
                        
                    if (data == nil || error != nil || statusCode >= 400 || statusCode < 200){
                        dispatch_async(dispatch_get_main_queue(), {
                            // TODO: handle error
                        })
                    } else {
                        let jsonResult: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error: error) as? NSDictionary
                            
                        dispatch_async(dispatch_get_main_queue(), {

                            if let userId = jsonResult["user_id"] as? String {
                                // we have a user id
                                let defaults = NSUserDefaults.standardUserDefaults()
                                defaults.setValue(userId, forKey: "userId")
                                defaults.synchronize()
                                
                                var next = self.storyboard?.instantiateViewControllerWithIdentifier("navbarController") as! UINavigationController
                                self.presentViewController(next, animated: true, completion: nil)
                            } else {
                                // seems like a new user, redirect to registration screen
                                var next = self.storyboard?.instantiateViewControllerWithIdentifier("registerView") as! RegisterViewController
                                self.presentViewController(next, animated: true, completion: nil)
                            }
                        })
                    }
                })
            }
        })
    }

    @IBAction func doLogin(sender: AnyObject) {
        var username = self.userNameInput.text
        var passwd = self.passwordInput.text
        var hasError = false
        
        if (username == "") {
            usernameErrorLabel.text = "Please enter your username"
            usernameErrorLabel.hidden = false
            hasError = true
        } else {
            usernameErrorLabel.hidden = true
        }
        
        if (passwd == "") {
            passwordErrorLabel.text = "Please enter your password"
            passwordErrorLabel.hidden = false
            hasError = true
        } else {
            passwordErrorLabel.hidden = true
        }
        
        if (!hasError){
            // lets talk to the server
            var url : String = "http://billion.lepunk.co.uk/api/"
            var request : NSMutableURLRequest = NSMutableURLRequest()
            var bodyData = "method=login&username=\(username)&password=\(passwd)"
            request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding);
            request.URL = NSURL(string: url)
            request.HTTPMethod = "POST"
            
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse!, data: NSData!, error: NSError!) -> Void in
                var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
                
                var statusCode = 0
                if let httpResponse = response as? NSHTTPURLResponse {
                    statusCode = httpResponse.statusCode
                }
                
                if (data == nil || error != nil || statusCode >= 400 || statusCode < 200){
                    dispatch_async(dispatch_get_main_queue(), {
                        self.usernameErrorLabel.text = "Network error, please try again"
                        self.usernameErrorLabel.hidden = false
                    })
                } else {
                    let jsonResult: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error: error) as? NSDictionary
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        if let userId = jsonResult["user_id"] as? String {
                            // received userId, save it and redirect
                            
                            let defaults = NSUserDefaults.standardUserDefaults()
                            defaults.setValue(userId, forKey: "userId")
                            defaults.synchronize()
                            
                            var next = self.storyboard?.instantiateViewControllerWithIdentifier("navbarController") as! UINavigationController
                            self.presentViewController(next, animated: true, completion: nil)
                            
                        } else if let jsonErrors = jsonResult["errors"] as? NSDictionary {
                            if let userError = jsonErrors["username"] as? String {
                                self.usernameErrorLabel.text = userError
                                self.usernameErrorLabel.hidden = false
                                hasError = true
                            }
                            
                            if let passwdError = jsonErrors["password"] as? String {
                                self.passwordErrorLabel.text = passwdError
                                self.passwordErrorLabel.hidden = false
                                hasError = true
                            }
                        }
                    })
                }
            })
        }
    }
    
    @IBAction func registerButtonClick(sender: AnyObject) {
        var next = self.storyboard?.instantiateViewControllerWithIdentifier("registerView") as! RegisterViewController
        self.presentViewController(next, animated: true, completion: nil)
    }
    
}

