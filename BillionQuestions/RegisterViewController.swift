//
//  SecondViewController.swift
//  BillionQuestions
//
//  Created by Tamas Szikszai on 17/05/2015.
//  Copyright (c) 2015 Tamas Szikszai. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    @IBOutlet weak var emailInput: UITextField!
    @IBOutlet weak var usernameInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var usernameErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var facebookLoginLabel: UILabel!
    @IBOutlet weak var facebookIdInput: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (FBSDKAccessToken.currentAccessToken() != nil){
            // User is already logged in, do work such as go to next view controller.
            self.populateUserData()
            let loginView : FBSDKLoginButton = FBSDKLoginButton()
            self.view.addSubview(loginView)
            loginView.center = self.view.center
            loginView.readPermissions = ["public_profile", "email", "user_friends"]
            loginView.delegate = self
        } else {
            let loginView : FBSDKLoginButton = FBSDKLoginButton()
            self.view.addSubview(loginView)
            loginView.center = self.view.center
            loginView.readPermissions = ["public_profile", "email", "user_friends"]
            loginView.delegate = self
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Facebook Delegate Methods
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        
        if ((error) != nil){
            // Process error
        } else if result.isCancelled {
            // Handle cancellations
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if result.grantedPermissions.contains("email") {
                self.populateUserData()
                // Do work
            }
        }
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        self.facebookLoginLabel.hidden = true
        self.facebookIdInput.text = ""
        self.emailInput.text = ""
        self.usernameInput.text = ""
    }
    
    func populateUserData() {
        self.facebookLoginLabel.hidden = false
        
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            
            if ((error) != nil) {
                // Process error
            } else {
                
                let facebookId : String = result.valueForKey("id") as! String
                var url : String = "http://billion.lepunk.co.uk/api/"
                var request : NSMutableURLRequest = NSMutableURLRequest()
                var bodyData = "method=facebookLogin&facebook_id=\(facebookId)"
                request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding);
                request.URL = NSURL(string: url)
                request.HTTPMethod = "POST"
                
                NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse!, data: NSData!, error: NSError!) -> Void in
                    var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
                    
                    var statusCode = 0
                    if let httpResponse = response as? NSHTTPURLResponse {
                        statusCode = httpResponse.statusCode
                    }
                    
                    if (data == nil || error != nil || statusCode >= 400 || statusCode < 200){
                        dispatch_async(dispatch_get_main_queue(), {
                            // TODO: handle error
                        })
                    } else {
                        let jsonResult: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error: error) as? NSDictionary
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            if let userId = jsonResult["user_id"] as? String {
                                // we have a user id
                                let defaults = NSUserDefaults.standardUserDefaults()
                                defaults.setValue(userId, forKey: "userId")
                                defaults.synchronize()
                                
                                var next = self.storyboard?.instantiateViewControllerWithIdentifier("navbarController") as! UINavigationController
                                self.presentViewController(next, animated: true, completion: nil)
                            } else {
                                // seems like a new user, redirect to registration screen
                                let userFullName : String = result.valueForKey("name") as! String
                                var stringlength = count(userFullName)
                                var ierror: NSError?
                                var regex:NSRegularExpression = NSRegularExpression(pattern: "([^a-zA-Z0-9_])", options: NSRegularExpressionOptions.CaseInsensitive, error: &ierror)!
                                var generatedUserName = regex.stringByReplacingMatchesInString(userFullName, options: nil, range: NSMakeRange(0, stringlength), withTemplate: "")
                                
                                self.usernameInput.text = generatedUserName
                                let userEmail : String = result.valueForKey("email") as! String
                                self.emailInput.text = userEmail
                                self.facebookIdInput.text = facebookId
                            }
                        })
                    }
                })
                
                // check if we already have this user
            }
        })
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,8}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    func isValidUsername(testStr:String) -> Bool {
        let userRegEx = "^[a-zA-Z_0-9]{5,}$"
        
        let userTest = NSPredicate(format:"SELF MATCHES %@", userRegEx)
        return userTest.evaluateWithObject(testStr)
    }
    
    @IBAction func doRegister(sender: AnyObject) {
        var email = self.emailInput.text
        var username = self.usernameInput.text
        var passwd = self.passwordInput.text
        var facebookId = self.facebookIdInput.text
        var hasError = false
        
        if (email == "") {
            emailErrorLabel.text = "Please enter your email address"
            emailErrorLabel.hidden = false
            hasError = true
        } else if (!self.isValidEmail(email)){
            emailErrorLabel.text = "Invalid email address"
            emailErrorLabel.hidden = false
            hasError = true
        } else {
            emailErrorLabel.hidden = true
        }
        
        if (username == "") {
            usernameErrorLabel.text = "Please pick a username"
            usernameErrorLabel.hidden = false
            hasError = true
        } else if (count(username)<5 || count(username)>30){
            usernameErrorLabel.text = "Username must be between 5 and 30 characters"
            usernameErrorLabel.hidden = false
            hasError = true
        } else if (!self.isValidUsername(username)){
            usernameErrorLabel.text = "Username can only contain alphanumeric characters"
            usernameErrorLabel.hidden = false
            hasError = true
        } else {
            usernameErrorLabel.hidden = true
        }
        
        if (passwd == "") {
            passwordErrorLabel.text = "Please pick a password"
            passwordErrorLabel.hidden = false
            hasError = true
        } else if (count(passwd)<5){
            passwordErrorLabel.text = "Password must be at least 5 characters long"
            passwordErrorLabel.hidden = false
            hasError = true
        } else {
            passwordErrorLabel.hidden = true
        }
        
        if (!hasError){
            // lets talk to the server
            var url : String = "http://billion.lepunk.co.uk/api/"
            var request : NSMutableURLRequest = NSMutableURLRequest()
            var bodyData = "method=register&username=\(username)&email=\(email)&password=\(passwd)&facebook_id=\(facebookId)"
            request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding);
            request.URL = NSURL(string: url)
            request.HTTPMethod = "POST"
            
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse!, data: NSData!, error: NSError!) -> Void in
                var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
                
                var statusCode = 0
                if let httpResponse = response as? NSHTTPURLResponse {
                    statusCode = httpResponse.statusCode
                }
                
                if (data == nil || error != nil || statusCode >= 400 || statusCode < 200){
                    dispatch_async(dispatch_get_main_queue(), {
                        self.emailErrorLabel.text = "Network error, please try again"
                        self.emailErrorLabel.hidden = false
                    })
                } else {
                    let jsonResult: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error: error) as? NSDictionary
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        if let userId = jsonResult["user_id"] as? String {
                            // have a new userId save it and go to the game
                            
                            let defaults = NSUserDefaults.standardUserDefaults()
                            defaults.setValue(userId, forKey: "userId")
                            defaults.synchronize()
                            
                            var next = self.storyboard?.instantiateViewControllerWithIdentifier("navbarController") as! UINavigationController
                            self.presentViewController(next, animated: true, completion: nil)
                            
                        } else if let jsonErrors = jsonResult["errors"] as? NSDictionary {
                            if let userError = jsonErrors["username"] as? String {
                                self.usernameErrorLabel.text = userError
                                self.usernameErrorLabel.hidden = false
                                hasError = true
                            }
                            
                            if let emailError = jsonErrors["email"] as? String {
                                self.emailErrorLabel.text = emailError
                                self.emailErrorLabel.hidden = false
                                hasError = true
                            }
                            
                            if let passwdError = jsonErrors["password"] as? String {
                                self.passwordErrorLabel.text = passwdError
                                self.passwordErrorLabel.hidden = false
                                hasError = true
                            }
                        }
                    })
                }
                
            })
        }
    }

    @IBAction func loginButtonClick(sender: AnyObject) {
        var next = self.storyboard?.instantiateViewControllerWithIdentifier("loginView") as! LoginViewController
        self.presentViewController(next, animated: true, completion: nil)
    }
    
    
}

