//
//  SecondViewController.swift
//  BillionQuestions
//
//  Created by Tamas Szikszai on 17/05/2015.
//  Copyright (c) 2015 Tamas Szikszai. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.checkLoggedUser()
    }
    
    func checkLoggedUser(){
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if let userId = defaults.valueForKey("userId") as? String {
            // we have a logged userId redirect to the game
            
            var next = self.storyboard?.instantiateViewControllerWithIdentifier("navbarController") as! UINavigationController
            self.presentViewController(next, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func registerButtonClick(sender: AnyObject) {
        var next = self.storyboard?.instantiateViewControllerWithIdentifier("registerView") as! RegisterViewController
        self.presentViewController(next, animated: true, completion: nil)
    }
    
    @IBAction func loginButtonClick(sender: AnyObject) {
        var next = self.storyboard?.instantiateViewControllerWithIdentifier("loginView") as! LoginViewController
        self.presentViewController(next, animated: true, completion: nil)
        
    }
    
}
