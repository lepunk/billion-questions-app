//
//  SecondViewController.swift
//  BillionQuestions
//
//  Created by Tamas Szikszai on 17/05/2015.
//  Copyright (c) 2015 Tamas Szikszai. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 1
        
        
        let barbuttonFont = UIFont(name: "Ubuntu-Light", size: 11) ?? UIFont.systemFontOfSize(11)
        
        UITabBar.appearance().tintColor = UIColor.whiteColor()
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: barbuttonFont, NSForegroundColorAttributeName:UIColor.whiteColor()], forState: UIControlState.Normal)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

