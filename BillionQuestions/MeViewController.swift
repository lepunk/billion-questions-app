//
//  SecondViewController.swift
//  BillionQuestions
//
//  Created by Tamas Szikszai on 17/05/2015.
//  Copyright (c) 2015 Tamas Szikszai. All rights reserved.
//

import UIKit

class MeViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.tabBarController?.title = "Statistics"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doLogout(sender: AnyObject) {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.removeObjectForKey("userId")
        defaults.synchronize()
        
        var next = self.storyboard?.instantiateViewControllerWithIdentifier("homeView") as! UIViewController
        self.presentViewController(next, animated: true, completion: nil)
    }
    
}

